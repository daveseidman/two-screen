require('dotenv').config({ path: '../.env' });

const randomID = () => [...Array(4)].map(() => String.fromCharCode(65 + Math.floor(Math.random() * 26))).join('');
const maxGuests = parseInt(process.env.MAX_GUESTS, 10) || 1;

// TODO: there's probably a simpler ES6 way to do this:
const idIsUnique = (code) => {
  let unique = true;
  for (let i = 0; i < Object.keys(rooms).length; i += 1) {
    if (code === rooms[Object.keys(rooms)[i]].code) unique = false;
    break;
  }
  return unique;
};


const createRoom = (socket) => {
  let id = randomID();
  while (!idIsUnique) id = randomID();
  const room = {
    id,
    host: socket,
    guests: [],
    status: 'waiting',
    maxGuests
  };

  return room;
};

module.exports = {
  createRoom
}