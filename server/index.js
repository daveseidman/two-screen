require('dotenv').config({ path: '../.env' });

const express = require('express');
const fs = require('fs');
const https = require('https');
const http = require('http');
const localIP = require('my-local-ip')();
const socketIo = require('socket.io');
const { createRoom } = require('./utils.js');
const app = express();

const port = parseInt(process.env.SERVER_PORT, 10);
const rooms = {};

const server =
  https.createServer({
    key: fs.readFileSync('../key.pem'),
    cert: fs.readFileSync('../cert.pem'),
  }, app);

const io = socketIo(server);

// const io = require('socket.io').listen(server);

server.listen(port, () => {
  console.log(`pilot server listening on ${port}`);
});

io.on('connection', (socket) => {
  const { query } = socket.handshake;

  // this is a host screen, create a room
  if (query.screen === '1') {
    const room = createRoom(socket.id);
    rooms[room.id] = room;
    socket.emit('room', room);
    console.log(rooms);
  }
  // this is a guest screen, do nothing, it will fire the join event immediately below
  if (query.screen === '2') {
    // console.log('second screen coming with code', query)
  }

  // attempt to join a room
  socket.on('join', (data) => {
    const room = rooms[data.id];
    if (!room)
      return io.to(socket.id).emit('joined', { success: false, message: 'room does not exist' })

    if (room.guests.length === room.maxGuests)
      return io.to(socket.id).emit('joined', { success: false, message: 'room is full' })

    room.guests.push(socket.id);
    io.to(socket.id).emit('joined', { success: true });
    console.log({ rooms })
  })

  socket.on('disconnect', () => {

    console.log('socket', socket.id, 'is leaving');
    let room = null;
    Object.keys(rooms).forEach((key) => {
      if (rooms[key].guests.indexOf(socket.id) >= 0) {
        console.log('removing a guest from room', key);
        rooms[key].guests.splice(rooms[key].guests.indexOf(socket.id), 1);
      }

      if (rooms[key].host === socket.id) {
        room = rooms[key];
        console.log('host is leaving, remove all guests from room', key);
        room.guests.forEach(guest => {
          io.to(guest).emit('joined', { success: false, message: 'host has left' })
        });
        delete rooms[key];
      }
    })
    console.log(room);
  })
  //   const type = getTypeBySocket(socket.id);
  //   console.log(chalk.red(`${type} disconnected: ${getRoomBySocket(socket.id) ? getRoomBySocket(socket.id).code : ''}`));
  //   // TODO: send signals to controller or airplane if it's companion disconnected
  //   if (type === 'airplane') {
  //     console.log('controls socket from:', rooms[socket.id].controls);
  //     io.to(rooms[socket.id].controls).emit('airplaneLeft');
  //     delete (rooms[socket.id]);
  //   }
  // });

});