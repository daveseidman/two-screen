import React, { useEffect, useRef, useState } from 'react';
import mobile from 'is-mobile';
import io from 'socket.io-client';
import FirstScreen from './views/FirstScreen';
import SecondScreen from './views/SecondScreen';

import './index.scss';

function App() {
  const [connected, setConnected] = useState(false);
  const screenNumber = mobile() ? 2 : 1;

  // useEffect(() => {
  const socket = io(`?screen=${screenNumber}`); //`?type=${type}${code ? `&code=${code}` : ''}&os=${os}`);
  // console.log({ socket })
  // return socket;
  // }, []);

  return (
    <div className="app">
      <h1>Two Screen</h1>
      {screenNumber === 1 && <FirstScreen socket={socket} />}
      {screenNumber === 2 && <SecondScreen socket={socket} />}
    </div>
  );
}

export default App;
