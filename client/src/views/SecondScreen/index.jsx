import React from 'react';
import './index.scss';

function SecondScreen(props) {
  const { socket } = props;

  socket.on('joined', (e) => {
    console.log('joined?');
    console.log(e)
  })

  const checkID = (e) => {
    if (e.target.value.length === 4) {
      socket.emit('join', { id: e.target.value.toUpperCase() })
    }
  }

  return (
    <div className="screen second">
      <h1>Second Screen</h1>
      <input
        className="roomid"
        type="text"
        placeholder="room id"
        maxLength={4}
        onChange={checkID}></input>
    </div>
  )
}

export default SecondScreen;