import react from '@vitejs/plugin-react';
// import basicSsl from '@vitejs/plugin-basic-ssl'
import { defineConfig } from 'vite';
import fs from 'fs';

export default defineConfig({
  plugins: [
    react(),
    // basicSsl(),
  ],
  server: {
    port: 8080,
    host: true,
    https: {
      key: fs.readFileSync('../key.pem'),
      cert: fs.readFileSync('../cert.pem'),
    },
    proxy: {
      '/socket.io': {
        target: 'https://localhost:8000',  // Your Express server URL. TODO: pull from .env
        ws: true,
        secure: false,
        changeOrigin: true,
      },
    },
  },
});
