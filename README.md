# Two Screen

Why don’t we see more Two-Screen experiences? Everyone has a smartphone, it’s almost always connected to the internet, almost every major store has screens, people like touch-free interactions (touching your own phone vs a public touchscreen).

My hypothesis is unfortunately that, well, it’s hard! It’s really hard to get running and even harder in a local development environment. The goal of this repo is to remove that difficulty and allow developers to create amazing two-screen experiences with ease!

## Getting Started

If you want to run over https you'll need to generate a key and cert which you'll share with you vite-dev-server and your express server by using the following command:

`mkcert -key-file key.pem -cert-file cert.pem localhost 192.168.1.100`

replace 192.168.1.100 with whatever your device typically gets for an address on your local network, do not skip this part. If your IP changes you can run this again and add the new address.

The first time you visit the app in your browser you'll have to trust the certificate, later attempts should work fine.

Run these commands in separate terminal windows or tabs, note you CAN combine them but it'll be easier to debug the flow of data if you keep them separate.
